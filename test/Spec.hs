import Data.Vector.Packed as PV
import Data.Vector.Generic as GV
import GHC.Word
import Test.Hspec
import Test.QuickCheck


main :: IO ()
main = hspec $ do
  describe "Packed" $ do
    it "sanity checks" $ property \(xs :: [ (Int , Word16 , Float) ]) ->
      toList (GV.fromList xs :: PV.Vector (Int , Word16 , Float)) == xs

