-- | Packed is similar to storable and unboxed, in that it gives you a typed array buffer with no pointer indirection for the elements. Unlike storable, it is backed by unpinned memory, and unlike unboxed, the product instances place tuples together in a single buffer, rather than allocating distinct buffers for each component. All encodings are byte-level, e.g., @Maybe Word8@ uses two bytes.
-- 
-- To use packed, simply derive a @Pack@ instance for your data type using GHC Generics and proceed to use the vector with the API provided by @Data.Vector.Generic@.
--
-- Example:
-- 
-- @
-- import Data.Vector.Packed as PV
-- import Data.Vector.Generic as GV
-- import GHC.Generics
-- 
--
-- data MyType = MyType !Int !Double
--   deriving (Generic,PV.Pack)
--
-- ex :: PV.Vector MyType
-- ex = GV.fromList [ MyType 1 2.0 , MyType 3 4.0 ]
-- @

module Data.Vector.Packed
       ( Pack(..)
       , Vector
       ) where

import Data.Vector.Packed.Internal

