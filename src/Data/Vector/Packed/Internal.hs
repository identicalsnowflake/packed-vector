{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE StandaloneDeriving #-}

module Data.Vector.Packed.Internal where

import Control.DeepSeq
import Control.Monad.Primitive
import Data.Bits
import Data.Functor.Const
import Data.Functor.Identity
import qualified Data.Primitive.ByteArray as BA
import Data.Semigroup
import qualified Data.Vector.Fusion.Bundle as Bundle
import qualified Data.Vector.Generic as GV
import qualified Data.Vector.Generic.Mutable as MGV
import qualified Data.Vector.Primitive.Mutable as MPR
import Foreign.C.Types
import Foreign.Ptr
import GHC.Exts hiding (toList)
import GHC.Generics
import GHC.Int
import GHC.Word
import qualified GHC.Exts as Exts (IsList(..))
import System.Posix.Types
import Text.Read (Read(..) , readListPrecDefault)


class Pack a where
  sizeInBytes :: Int
  {-# INLINE sizeInBytes #-}
  default sizeInBytes :: GPack a (Rep a) => Int
  sizeInBytes = gsizeInBytes @a @(Rep a)
  
  readByteArray :: ByteArray# -> Int# -> a -- offset in bytes
  {-# INLINEABLE readByteArray #-}
  default readByteArray :: (Generic a , GPack a (Rep a)) => ByteArray# -> Int# -> a
  readByteArray b i = to do greadByteArray @a @(Rep a) b i

  readMutableByteArray :: MutableByteArray# s -> Int# -> State# s -> (# State# s , a #)
  {-# INLINE readMutableByteArray #-}
  default readMutableByteArray :: (Generic a , GPack a (Rep a))
                               => MutableByteArray# s -> Int# -> State# s -> (# State# s , a #)
  readMutableByteArray b i s = case greadMutableByteArray @a @(Rep a) b i s of
    (# s0 , a #) -> (# s0 , to a #)
  
  writeByteArray :: MutableByteArray# s -> Int# -> a -> State# s -> State# s -- offset in bytes
  {-# INLINEABLE writeByteArray #-}
  default writeByteArray :: (Generic a , GPack a (Rep a))
                         => MutableByteArray# s -> Int# -> a -> State# s -> State# s
  writeByteArray b i x s = gwriteByteArray @a @(Rep a) b i (from x) s

class GPack s f where
  gsizeInBytes :: Int
  greadByteArray :: ByteArray# -> Int# -> f a
  greadMutableByteArray :: MutableByteArray# t -> Int# -> State# t -> (# State# t , f a #)
  gwriteByteArray :: MutableByteArray# t -> Int# -> f a -> State# t -> State# t

instance GPack s U1 where
  {-# INLINE gsizeInBytes #-}
  gsizeInBytes = 0
  {-# INLINE greadByteArray #-}
  greadByteArray _ _ = U1
  {-# INLINE greadMutableByteArray #-}
  greadMutableByteArray _ _ s = (# s , U1 #)
  {-# INLINE gwriteByteArray #-}
  gwriteByteArray _ _ _ s = s

instance Pack a => GPack s (K1 i a) where
  {-# INLINE gsizeInBytes #-}
  gsizeInBytes = sizeInBytes @a
  {-# INLINE greadByteArray #-}
  greadByteArray b i = K1 do readByteArray b i
  {-# INLINE greadMutableByteArray #-}
  greadMutableByteArray b i s = case readMutableByteArray b i s of
    (# s0 , x #) -> (# s0 , K1 x #)
  {-# INLINE gwriteByteArray #-}
  gwriteByteArray b i (K1 x) s = writeByteArray b i x s

instance (GPack s a , GPack s b) => GPack s (a :*: b) where
  {-# INLINE gsizeInBytes #-}
  gsizeInBytes = gsizeInBytes @s @a + gsizeInBytes @s @b
  {-# INLINE greadByteArray #-}
  greadByteArray b i = do
    let !(I# s) = gsizeInBytes @s @a
    greadByteArray @s b i :*: greadByteArray @s b (i +# s)
  {-# INLINE greadMutableByteArray #-}
  greadMutableByteArray b i s0 = let !(I# s) = gsizeInBytes @s @a in
    case greadMutableByteArray @s b i s0 of
      (# s1 , x #) -> case greadMutableByteArray @s b (i +# s) s1 of
        (# s2 , y #) -> (# s2 , x :*: y #)
  {-# INLINE gwriteByteArray #-}
  gwriteByteArray ba i (a :*: b) s = do
    let !(I# sz) = gsizeInBytes @s @a
    gwriteByteArray @s ba (i +# sz) b (gwriteByteArray @s ba i a s)

instance (GPack s a , GPack s b) => GPack s (a :+: b) where
  {-# INLINE gsizeInBytes #-}
  gsizeInBytes = 1 + max (gsizeInBytes @s @a) (gsizeInBytes @s @b)
  {-# INLINE greadByteArray #-}
  greadByteArray b i = case readByteArray b i :: Word8 of
    0 -> L1 (greadByteArray @s b (i +# 1#))
    _ -> R1 (greadByteArray @s b (i +# 1#))
  {-# INLINE greadMutableByteArray #-}
  greadMutableByteArray b i s = case readMutableByteArray b i s of
    (# s0 , (0 :: Word8) #) -> case greadMutableByteArray @s b (i +# 1#) s0 of
      (# s1 , v #) -> (# s1 , L1 v #)
    (# s0 , _ #) -> case greadMutableByteArray @s b (i +# 1#) s0 of
      (# s1 , v #) -> (# s1 , R1 v #)
  {-# INLINE gwriteByteArray #-}
  gwriteByteArray b i k s = case k of
    L1 x -> gwriteByteArray @s b (i +# 1#) x do writeByteArray b i (0 :: Word8) s
    R1 x -> gwriteByteArray @s b (i +# 1#) x do writeByteArray b i (1 :: Word8) s

instance GPack s a => GPack s (M1 i t a) where
  {-# INLINE gsizeInBytes #-}
  gsizeInBytes = gsizeInBytes @s @a
  {-# INLINE greadByteArray #-}
  greadByteArray b i = M1 do greadByteArray @s b i
  {-# INLINE greadMutableByteArray #-}
  greadMutableByteArray b i s = case greadMutableByteArray @s b i s of
    (# s0 , v #) -> (# s0 , M1 v #)
  {-# INLINE gwriteByteArray #-}
  gwriteByteArray b i (M1 x) s = gwriteByteArray @s b i x s



instance Pack Word8 where
  {-# INLINE sizeInBytes #-}
  sizeInBytes = 1
  {-# INLINE readByteArray #-}
  readByteArray ba i = W8# (indexWord8Array# ba i)
  {-# INLINE readMutableByteArray #-}
  readMutableByteArray mba i s = case readWord8Array# mba i s of
    (# s0 , w #) -> (# s0 , W8# w #)
  {-# INLINE writeByteArray #-}
  writeByteArray ba i (W8# x) s =
    writeWord8Array# ba i x s

instance Pack Word16 where
  {-# INLINE sizeInBytes #-}
  sizeInBytes = 2
  {-# INLINE readByteArray #-}
  readByteArray ba i = W16# (indexWord8ArrayAsWord16# ba i)
  {-# INLINE readMutableByteArray #-}
  readMutableByteArray mba i s = case readWord8ArrayAsWord16# mba i s of
    (# s0 , w #) -> (# s0 , W16# w #)
  {-# INLINE writeByteArray #-}
  writeByteArray ba i (W16# x) s =
    writeWord8ArrayAsWord16# ba i x s

instance Pack Word32 where
  {-# INLINE sizeInBytes #-}
  sizeInBytes = 4
  {-# INLINE readByteArray #-}
  readByteArray ba i = W32# (indexWord8ArrayAsWord32# ba i)
  {-# INLINE readMutableByteArray #-}
  readMutableByteArray mba i s = case readWord8ArrayAsWord32# mba i s of
    (# s0 , w #) -> (# s0 , W32# w #)
  {-# INLINE writeByteArray #-}
  writeByteArray ba i (W32# x) s =
    writeWord8ArrayAsWord32# ba i x s

instance Pack Word64 where
  {-# INLINE sizeInBytes #-}
  sizeInBytes = 8
  {-# INLINE readByteArray #-}
  readByteArray ba i = W64# (indexWord8ArrayAsWord64# ba i)
  {-# INLINE readMutableByteArray #-}
  readMutableByteArray mba i s = case readWord8ArrayAsWord64# mba i s of
    (# s0 , w #) -> (# s0 , W64# w #)
  {-# INLINE writeByteArray #-}
  writeByteArray ba i (W64# x) s =
    writeWord8ArrayAsWord64# ba i x s

instance Pack Int8 where
  {-# INLINE sizeInBytes #-}
  sizeInBytes = 1
  {-# INLINE readByteArray #-}
  readByteArray ba i = I8# (indexInt8Array# ba i)
  {-# INLINE readMutableByteArray #-}
  readMutableByteArray mba i s = case readInt8Array# mba i s of
    (# s0 , w #) -> (# s0 , I8# w #)
  {-# INLINE writeByteArray #-}
  writeByteArray ba i (I8# x) s =
    writeInt8Array# ba i x s

instance Pack Int16 where
  {-# INLINE sizeInBytes #-}
  sizeInBytes = 2
  {-# INLINE readByteArray #-}
  readByteArray ba i = I16# (indexWord8ArrayAsInt16# ba i)
  {-# INLINE readMutableByteArray #-}
  readMutableByteArray mba i s = case readWord8ArrayAsInt16# mba i s of
    (# s0 , w #) -> (# s0 , I16# w #)
  {-# INLINE writeByteArray #-}
  writeByteArray ba i (I16# x) s =
    writeWord8ArrayAsInt16# ba i x s

instance Pack Int32 where
  {-# INLINE sizeInBytes #-}
  sizeInBytes = 4
  {-# INLINE readByteArray #-}
  readByteArray ba i = I32# (indexWord8ArrayAsInt32# ba i)
  {-# INLINE readMutableByteArray #-}
  readMutableByteArray mba i s = case readWord8ArrayAsInt32# mba i s of
    (# s0 , w #) -> (# s0 , I32# w #)
  {-# INLINE writeByteArray #-}
  writeByteArray ba i (I32# x) s =
    writeWord8ArrayAsInt32# ba i x s

instance Pack Int64 where
  {-# INLINE sizeInBytes #-}
  sizeInBytes = 8
  {-# INLINE readByteArray #-}
  readByteArray ba i = I64# (indexWord8ArrayAsInt64# ba i)
  {-# INLINE readMutableByteArray #-}
  readMutableByteArray mba i s = case readWord8ArrayAsInt64# mba i s of
    (# s0 , w #) -> (# s0 , I64# w #)
  {-# INLINE writeByteArray #-}
  writeByteArray ba i (I64# x) s =
    writeWord8ArrayAsInt64# ba i x s

instance Pack Float where
  {-# INLINE sizeInBytes #-}
  sizeInBytes = 4
  {-# INLINE readByteArray #-}
  readByteArray ba i = F# (indexWord8ArrayAsFloat# ba i)
  {-# INLINE readMutableByteArray #-}
  readMutableByteArray mba i s = case readWord8ArrayAsFloat# mba i s of
    (# s0 , w #) -> (# s0 , F# w #)
  {-# INLINE writeByteArray #-}
  writeByteArray ba i (F# x) s =
    writeWord8ArrayAsFloat# ba i x s

instance Pack Double where
  {-# INLINE sizeInBytes #-}
  sizeInBytes = 8
  {-# INLINE readByteArray #-}
  readByteArray ba i = D# (indexWord8ArrayAsDouble# ba i)
  {-# INLINE readMutableByteArray #-}
  readMutableByteArray mba i s = case readWord8ArrayAsDouble# mba i s of
    (# s0 , w #) -> (# s0 , D# w #)
  {-# INLINE writeByteArray #-}
  writeByteArray ba i (D# x) s =
    writeWord8ArrayAsDouble# ba i x s

instance Pack Int where
  {-# INLINE sizeInBytes #-}
  sizeInBytes = finiteBitSize (0 :: Int)
  {-# INLINE readByteArray #-}
  readByteArray ba i = I# (indexWord8ArrayAsInt# ba i)
  {-# INLINE readMutableByteArray #-}
  readMutableByteArray mba i s = case readWord8ArrayAsInt# mba i s of
    (# s0 , w #) -> (# s0 , I# w #)
  {-# INLINE writeByteArray #-}
  writeByteArray ba i (I# x) s =
    writeWord8ArrayAsInt# ba i x s

instance Pack Word where
  {-# INLINE sizeInBytes #-}
  sizeInBytes = finiteBitSize (0 :: Word)
  {-# INLINE readByteArray #-}
  readByteArray ba i = W# (indexWord8ArrayAsWord# ba i)
  {-# INLINE readMutableByteArray #-}
  readMutableByteArray mba i s = case readWord8ArrayAsWord# mba i s of
    (# s0 , w #) -> (# s0 , W# w #)
  {-# INLINE writeByteArray #-}
  writeByteArray ba i (W# x) s =
    writeWord8ArrayAsWord# ba i x s

instance (Pack a , Pack b) => Pack (a , b)
instance (Pack a , Pack b , Pack c) => Pack (a , b , c)
instance (Pack a , Pack b , Pack c , Pack d) => Pack (a , b , c , d)
instance (Pack a , Pack b , Pack c , Pack d , Pack e) => Pack (a , b , c , d , e)
instance Pack a => Pack (Const a b)
instance (Pack a , Pack b) => Pack (Either a b)
instance Pack a => Pack (Identity a)
instance Pack a => Pack (Maybe a)
instance Pack ()
instance Pack Bool


data MVector s a = MVector {
    mbuffer :: !(MutableByteArray# s)
  , moffset :: {-# UNPACK #-} !Int -- in bytes
  , mcount :: {-# UNPACK #-} !Int
  }

instance Pack a => MGV.MVector MVector a where
  {-# INLINE basicLength #-}
  basicLength = mcount

  {-# INLINE basicUnsafeSlice #-}
  basicUnsafeSlice s l (MVector mb mo _) = MVector mb (mo + s * sizeInBytes @a) l

  {-# INLINE basicOverlaps #-}
  basicOverlaps (MVector mba1 mo1 mc1) (MVector mba2 mo2 mc2) = case sameMutableByteArray# mba1 mba2 of
    0# -> False
    _ -> do
      let e1 = mo1 + mc1 * sizeInBytes @a
          e2 = mo2 + mc2 * sizeInBytes @a
      bet mo1 mo2 e2 || bet e1 mo2 e2
    where
      bet x a b = x >= a && x <= b

  {-# INLINE basicUnsafeNew #-}
  basicUnsafeNew i = primitive \s -> do
    let !(I# c) = i * sizeInBytes @a
        !(# s1 , mba #) = newByteArray# c s
    (# s1 , MVector mba 0 i #)

  {-# INLINE basicInitialize #-}
  basicInitialize (MVector mb mo ml) =
    MGV.basicInitialize @_ @Word8 (MPR.MVector mo (ml * sizeInBytes @a) (BA.MutableByteArray mb))

  {-# INLINE basicUnsafeRead #-}
  basicUnsafeRead (MVector mb mo _) i = primitive \s -> do
    let !(I# o) = mo + i * sizeInBytes @a
    readMutableByteArray mb o s

  {-# INLINE basicUnsafeWrite #-}
  basicUnsafeWrite (MVector mb mo _) i v = primitive \s -> do
    let !(I# o) = mo + i * sizeInBytes @a
        s' = writeByteArray mb o v s
    (# s' , () #)

  {-# INLINE basicUnsafeCopy #-}
  basicUnsafeCopy (MVector tmb (I# to_) _) (MVector smb (I# so) c) = primitive \s -> do
    case c * sizeInBytes @a of
      I# cb -> (# copyMutableByteArray# smb so tmb to_ cb s , () #)

  {-# INLINE basicUnsafeMove #-}
  basicUnsafeMove (MVector tmb (I# to_) _) (MVector smb (I# so) c) = primitive \s -> do
    case c * sizeInBytes @a of
      I# cb -> (# copyMutableByteArray# smb so tmb to_ cb s , () #)

type instance GV.Mutable Vector = MVector


data Vector a = Vector {
    buffer :: !ByteArray#
  , offset :: {-# UNPACK #-} !Int -- in bytes
  , count :: {-# UNPACK #-} !Int
  }

instance Pack a => GV.Vector Vector a where
  {-# INLINE basicUnsafeFreeze #-}
  basicUnsafeFreeze (MVector mba o c) = primitive \s -> do
    let !(# s1 , ba #) = unsafeFreezeByteArray# mba s
    (# s1 , Vector ba o c #)

  {-# INLINE basicUnsafeThaw #-}
  -- this does not seem to me justified unless @basicUnsafeNew@ is changed to pinned,
  -- but the @array@ package that comes bundled with GHC uses this, so...
  basicUnsafeThaw (Vector ba o c) = pure (MVector (unsafeCoerce# ba) o c)

  {-# INLINE basicLength #-}
  basicLength (Vector _ _ c) = c

  {-# INLINE basicUnsafeSlice #-}
  basicUnsafeSlice s l (Vector ba o _) = Vector ba (o + sizeInBytes @a * s) l

  {-# INLINE basicUnsafeIndexM #-}
  basicUnsafeIndexM (Vector ba o _) i = case o + i * sizeInBytes @a of
    I# k -> pure (readByteArray ba k)

  {-# INLINE basicUnsafeCopy #-}
  basicUnsafeCopy (MVector mba (I# o1) _) (Vector ba (I# o2) c) = primitive \s ->
    case c * sizeInBytes @a of
      I# cb -> (# copyByteArray# ba o2 mba o1 cb s , () #)

instance Pack a => Exts.IsList (Vector a) where
  type Item (Vector a) = a
  {-# INLINE fromList #-}
  fromList = GV.fromList
  {-# INLINE fromListN #-}
  fromListN = GV.fromListN
  {-# INLINE toList #-}
  toList = GV.toList

instance NFData a => NFData (Vector a) where
  {-# INLINE rnf #-}
  rnf = const ()

instance NFData1 Vector where
  {-# INLINE liftRnf #-}
  liftRnf _ _ = ()

instance Pack a => Semigroup (Vector a) where
  {-# INLINE (<>) #-}
  (<>) = (GV.++)
  {-# INLINE sconcat #-}
  sconcat = GV.concatNE

instance Pack a => Monoid (Vector a) where
  {-# INLINE mempty #-}
  mempty = GV.empty
  {-# INLINE mconcat #-}
  mconcat = GV.concat

instance (Pack a , Eq a) => Eq (Vector a) where
  {-# INLINE (==) #-}
  xs == ys = Bundle.eq (GV.stream xs) (GV.stream ys)
  {-# INLINE (/=) #-}
  xs /= ys = not (Bundle.eq (GV.stream xs) (GV.stream ys))

instance (Pack a , Ord a) => Ord (Vector a) where
  {-# INLINE compare #-}
  compare xs ys = Bundle.cmp (GV.stream xs) (GV.stream ys)
  {-# INLINE (<) #-}
  xs < ys = Bundle.cmp (GV.stream xs) (GV.stream ys) == LT
  {-# INLINE (<=) #-}
  xs <= ys = Bundle.cmp (GV.stream xs) (GV.stream ys) /= GT
  {-# INLINE (>) #-}
  xs > ys = Bundle.cmp (GV.stream xs) (GV.stream ys) == GT
  {-# INLINE (>=) #-}
  xs >= ys = Bundle.cmp (GV.stream xs) (GV.stream ys) /= LT

instance (Pack a , Show a) => Show (Vector a) where
  {-# INLINE showsPrec #-}
  showsPrec = GV.showsPrec

instance (Pack a , Read a) => Read (Vector a) where
  {-# INLINE readPrec #-}
  readPrec = GV.readPrec
  {-# INLINE readListPrec #-}
  readListPrec = readListPrecDefault


deriving newtype instance Pack IntPtr
deriving newtype instance Pack WordPtr
deriving newtype instance Pack CUIntMax
deriving newtype instance Pack CIntMax
deriving newtype instance Pack CUIntPtr
deriving newtype instance Pack CIntPtr
deriving newtype instance Pack CSigAtomic
deriving newtype instance Pack CWchar
deriving newtype instance Pack CSize
deriving newtype instance Pack CPtrdiff
deriving newtype instance Pack CBool
deriving newtype instance Pack CULLong
deriving newtype instance Pack CLLong
deriving newtype instance Pack CULong
deriving newtype instance Pack CLong
deriving newtype instance Pack CUInt
deriving newtype instance Pack CInt
deriving newtype instance Pack CUShort
deriving newtype instance Pack CShort
deriving newtype instance Pack CUChar
deriving newtype instance Pack CSChar
deriving newtype instance Pack CChar
deriving newtype instance Pack Fd
deriving newtype instance Pack CNfds
deriving newtype instance Pack CSocklen
deriving newtype instance Pack CKey
deriving newtype instance Pack CId
deriving newtype instance Pack CFsFilCnt
deriving newtype instance Pack CFsBlkCnt
deriving newtype instance Pack CClockId
deriving newtype instance Pack CBlkCnt
deriving newtype instance Pack CBlkSize
deriving newtype instance Pack CRLim
deriving newtype instance Pack CTcflag
deriving newtype instance Pack CUid
deriving newtype instance Pack CNlink
deriving newtype instance Pack CGid
deriving newtype instance Pack CSsize
deriving newtype instance Pack CPid
deriving newtype instance Pack COff
deriving newtype instance Pack CMode
deriving newtype instance Pack CIno
deriving newtype instance Pack CDev

